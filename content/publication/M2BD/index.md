+++
title = "M2 Big Data"
date = 2019-01-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Tidyverse, Calcul parallèle, Réseaux de neurones, Projets"

summary = "Tidyverse, Calcul parallèle, Réseaux de neurones, Projets"

# Caption (optional)
caption = " "

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["mecen","AD","master","R"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Ggplot2", url = "M2-CM-R-Graphics-ggplot2.pdf"}, 
{name = "Tidyverse", url = "M2-CM-R-Tidyverse.pdf"}, 
{name = "Shiny", url = "M2-CM-R-AppliShiny.pdf"}, 
{name = "HPC", url = "M2-CM-HighPerformanceComputing.pdf"},
{name = "Réseaux de neurones", url = "M2-CM-ReseauxNeurones.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

## Objectifs

Ce cours est l'occasion d'aborder divers thèmes théoriques ou techniques selon les demandes et les besoins.

## Plan du cours

En 2018-2019

Julie Scholler

* Graphiques avancés sous R avec Ggplot2
* R : Tidyverse, flexidashboard, applications Shiny
* High Performance Computing : quelques éléments sous R, survol d'autres solutions (Hadoop, Spark)
* Réseaux de neurones

Franck Piller

* Support Vector Machine
* Analyse textuelle

Projets étudiants

* Graphiques interactifs, cartes interactives
* Analyse textuelle et utilisation des packages twiteR et Rlinkedin
