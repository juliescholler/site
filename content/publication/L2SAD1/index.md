+++
title = "L2 Statistiques et Analyse de Données 1"
date = 2018-12-20T00:00:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Julie Scholler"]

# Publication type.
# Legend:
#  0: divers
#  1: cours de licence
#  2: cours de master
#  3: Manuscrit
#  4: Technical report
#  5: Livre
#  6: Tutoriel
publication_types = ["5"]

# Publication name and optional abbreviated version.
publication = ""
publication_short = " "

# Abstract and optional shortened version.
abstract = "Échantillonnage et estimation"

summary = "Échantillonnage et estimation"

# Caption (optional)
caption = "Photo by [Academic](https://sourcethemes.com/academic/)"

# Focal point (optional)
# Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
focal_point = "Center"
  
# Show image only in page previews?
preview_only = false

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's filename without extension.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
#   Otherwise, set `projects = []`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["lecot","stats","licence","L2"]

# Links (optional).
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
links = [{name = "Polycopié", url = "L2-AD1-poly-1819.pdf"}, 
{name = "Exercices", url = "L2-AD1-fasc-exos-etu.pdf"}, 
{name = "CM-Thème 1", url = "L2ECO-S1-CM-theme1.pdf"}, 
{name = "CM-Thème 2", url = "L2ECO-S1-CM-theme2.pdf"},
{name = "Pré Requis - Polycopié", url = "L1ECO-CoursComplet-1718.pdf"},
{name = "Pré Requis - Exercices", url = "L1ECO-Stat2-Fascicule-1718.pdf"}]

# Does this page contain LaTeX math? (true/false)
math = true

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

## Objectifs

L'objectif de cet enseignement est d'approfondir la maîtrise des outils probabilistes utiles à la compréhension et à la bonne utilisation des méthodes de statistique inférentielle. Les principaux concepts de statistique inférentielle seront utilisés pour estimer les valeurs des paramètres d'une population, sur la base de résultats d'échantillon.
Ce sera l'occasion de favoriser l'analyse « critique » des données chiffrées issues des probabilités et de la statistique inférentielle.


## Pré requis

* vocabulaire de statistique descriptive : population, individu, types de variables (quantitative ou qualitative)
* manipulation de l'espérance et de la variance (très important)
* connaissance des lois classiques : Bernoulli, binomiale, normale

## Plan du cours

* Couples et vecteurs de variables aléatoires, vecteurs gaussiens
* Échantillonnage : échantillon, statistique, moyenne empirique, variance empirique, statistiques d'ordre * Théorie de l'estimation ponctuelle :
	biais, erreur quadratique moyenne, comparaison d'estimateur, méthode des moments, méthode du maximum de vraisemblance.

