+++
# Slider widget.
widget = "slider"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = false  # Activate this widget? true/false
weight = 1  # Order that this section will appear.

# Slide interval.
# Use `false` to disable animation or enter a time in ms, e.g. `5000` (5s).
interval = false

# Slides.
# Duplicate an `[[item]]` block to add more slides.
[[item]]
  title = "Bienvenue"#:smile:
  content = "Vous trouverez sur ce site des documents relatifs aux enseignements dans lesquels j’interviens à l’Université de Tours. Les documents concernent les Statistiques et les Mathématiques pour économistes, ainsi que le logiciel R et LateX. [L1](/lecot/#lecot1) [L2](/lecot/#lecot2) [L3](/lecot/#lecot3) [Master](/mecen/)"
  align = "left"  # Choose `center`, `left`, or `right`.

  # Overlay a color or image (optional).
  #   Deactivate an option by commenting out the line, prefixing it with `#`.
  overlay_color = "#45a59d"  # An HTML color value.
  overlay_img = "headers/texture-b.png"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.

  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  #cta_label = "Get Academic"
  #cta_url = "https://sourcethemes.com/academic/"
  #cta_icon_pack = "fas"
  #cta_icon = "graduation-cap"

[[item]]
  title = "MÉcEn"
  content = "Accéder directement au cours du master économiste d'entreprise."
  align = "left"  # Choose `center`, `left`, or `right`.

  # Overlay a color or image (optional).
  #   Deactivate an option by commenting out the line, prefixing it with `#`.
  overlay_color = "#45a59d"  # An HTML color value.
  overlay_img = "headers/texture-b.png"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.
  
  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  cta_label = "Liens vers les posts"
  cta_url = "#posts"
  cta_icon_pack = "fas"
  cta_icon = "graduation-cap"
  
[[item]]
  title = "LÉcoT"
  content = "Accéder directement au cours de la licence d'économie."
  align = "left"  # Choose `center`, `left`, or `right`.

  # Overlay a color or image (optional).
  #   Deactivate an option by commenting out the line, prefixing it with `#`.
  overlay_color = "#45a59d"  # An HTML color value.
  overlay_img = "headers/texture-b.png"  # Image path relative to your `static/img/` folder.
  overlay_filter = 0  # Darken the image. Value in range 0-1.
  
  # Call to action button (optional).
  #   Activate the button by specifying a URL and button label below.
  #   Deactivate by commenting out parameters, prefixing lines with `#`.
  cta_label = "Liens vers les posts"
  cta_url = "#posts"
  cta_icon_pack = "fas"
  cta_icon = "graduation-cap"
  
+++
